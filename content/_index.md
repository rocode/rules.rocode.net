+++
+++

![CraftHub TTT Logo](TTT.png)
# Server Rules
## RDM
1. Do not RDM (Random Deathmatch) or for a random reason outside of the current round/game.
2. Do not kill AFKs
3. Do not PropKill
4. Hacking or Cheating
5. Hacking, injecting, cheating or scripting will result in an immediate ban

## Ghosting or Metagaming
1. Giving a living player information regarding the current round while dead.

## Hateful Conduct
1. Racism.
2. Homophobia.
3. Sexual Harassment.
4. Prejudice Behaviour.
5. Any other form of disrespect directed towards another player.
6. Offensive username (Change when asked by staff)

## Spamming
1. Do not spam the mic, text, or radio chat commands. Staff will issue mutes for anyone abusing communication systems.

## Unreadable names
1. Names should be readable and pronounceable (Change when asked by staff)

## Traitor/Karma Baiting
1. Very simple rule: Don't act like a traitor if you're innocent in order for someone else to lose karma.

